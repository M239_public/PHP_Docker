# Content

This content is a fork of [https://github.com/taniarascia/pdo](https://github.com/taniarascia/pdo).
I just optimized it for my course M239 as a practical example for dockerizing an PHP-App.

# Original ReadME

## Connecting to MySQL in PHP using PDO

Create a Simple Database App: Connecting to MySQL with PHP

#### [View the tutorial](https://www.taniarascia.com/create-a-simple-database-app-connecting-to-mysql-with-php/)

### Lessons

- Install database and create table
- Submit users
- Query and filter users

### License

The code is open source and available under the [MIT License](LICENSE.md).
